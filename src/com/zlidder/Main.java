package com.zlidder;

import com.zlidder.model.World;
import com.zlidder.net.Server;

/**
 * Used for running the server and more
 * @author Fabrice L
 *
 */

public class Main {
	
	private static int serverPort = 43594;
	private static String serverName = "Zlidder v0.1";
	
	public static void main(String[] args) {
		/**
		 * Starts the server
		 */
		System.out.println("Starting " + getServerName() + "...");
		new Thread(getClientHandler()).start();
		
	}

	public static int getServerPort() {
		return serverPort;
	}

	public static String getServerName() {
		return serverName;
	}

	public static World getWorld() {
		return new World();
	}
	
	public static Server getClientHandler() {
		return new Server();
	}

}
