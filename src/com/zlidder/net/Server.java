package com.zlidder.net;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.zlidder.Main;

/**
 *
 * @author Fabrice L
 *
 */

public class Server implements Runnable {
	
	private static boolean shutdownClientHandler;
	private static ServerSocket clientListener;
	private static int maxConnections = 100000;
	private static List<String> connections = new ArrayList<String>(getMaxConnections());
	private static List<Integer> connectionCount = new ArrayList<Integer>(getMaxConnections());

	@Override
	public void run() {
		try {
			setShutdownClientHandler(false);
			setClientListener(new ServerSocket(Main.getServerPort(), 1, null));
			while(true) {
				Socket socket = acceptSocketSafe(getClientListener());
				socket.setTcpNoDelay(true);
				String connectingHost = socket.getInetAddress().getHostName();
				boolean isBanned = false;
				if (getClientListener() != null && !isBanned) {
					int hostFound = 0;
					if (getConnections().contains(connectingHost)) {
						hostFound += 1;
						System.out.println("Host found");
					}
					if (hostFound < 3) {
						Main.getWorld().newPlayerClient(socket, connectingHost);
					} else {
						socket.close();
					}
				} else {
					socket.close();
				}
			}
		} catch (IOException exception) {
			if (!isShutdownClientHandler()) {
				System.out.println("Unable to start up listener on port " + Main.getServerPort() + "!");
			} else {
				System.out.println("ClientHandler was shut down!");
			}
		}

	}
	
	public Socket acceptSocketSafe(ServerSocket serverSocket) {
		boolean socketFound = false;
		Socket socket = null;
		do {
			try {
				socket = serverSocket.accept();
				int i = socket.getInputStream().read();
				if ((i & 0xFF) == 14) {
					socketFound = true;
				}
			} catch (Exception e) {
		}
	} while (!socketFound);
		return socket;
	}

	public static boolean isShutdownClientHandler() {
		return shutdownClientHandler;
	}

	public static void setShutdownClientHandler(boolean shutdownClientHandler) {
		Server.shutdownClientHandler = shutdownClientHandler;
	}

	public static ServerSocket getClientListener() {
		return clientListener;
	}

	public static void setClientListener(ServerSocket clientListener) {
		Server.clientListener = clientListener;
	}

	public static List<String> getConnections() {
		return connections;
	}

	public static int getMaxConnections() {
		return maxConnections;
	}

	public static List<Integer> getConnectionCount() {
		return connectionCount;
	}
	
}
