package com.zlidder.model;

import java.net.Socket;

/**
 * Class were all world related stuff is handled
 * @author Fabrice L
 *
 */

public class World {
	
	public void newPlayerClient(Socket socket, String connectingHost) {
		System.out.println("Accepted connection from: " + connectingHost);
	}
	
}
